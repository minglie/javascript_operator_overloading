'use strict'

var webpack = require('webpack')
var path = require('path')

module.exports = {
    entry: __dirname + '/src/main.js',
    output: {
        path: __dirname + '/dist/',
        filename: 'ookay.js'
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                use: {
                    loader: 'babel-loader',
                },
                exclude: /node_modules/
            }
        ]
    }
}