import {translate_block} from "./ookay-core";

export function inject (type = 'class') {
    if (type = 'method') {
        return function (target, name, descriptor) {
            descriptor.value = new Function(translate_block(name, target.prototype[name].toString()))()
        }
    }else {
        return function (target) {
            let protos = Object.getOwnPropertyNames(target.prototype)
            protos.forEach( (proto, key, own) => {
                target.prototype[proto] = new Function(translate_block(proto, target.prototype[proto].toString()))()
            })
        }
    }
}