import OOkay from './OOkay'
import { __replace__ } from './ookay-core'
import { inject } from "./decorator";

((ookay) => {
    ookay.OOkay = OOkay
    ookay.__replace__ = __replace__
    ookay.inject = inject
    ookay.onload = () => {
        let scripts = document.querySelectorAll('script[type="text/okayscript"]')
        for (let i = 0,len = scripts.length;i < len;i++) {
            OOkay.__$$__(scripts[i].innerHTML).call(ookay)
        }
    }
})(window)