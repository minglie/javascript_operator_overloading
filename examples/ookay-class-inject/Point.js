class Point {
    constructor (x, y) {
        this.x = x
        this.y = y
    }
    static __add__(p1, p2) {
        return new Point(p1.x + p2.x, p1.y + p2.y)
    }
    static __plus__(p1, p2) {
        return new Point(p1.x - p2.x, p1.y - p2.y)
    }
}