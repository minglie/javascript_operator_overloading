class Rectangle {
    constructor (){
        OOkay.inject(this)
        this.left_top = new Point(0, 0)
        this.right_bottom = new Point(1, 1)
    }
    width () {
        let p3 = this.right_bottom - this.left_top
        return p3.x
    }
    height () {
        let p3 = this.right_bottom - this.left_top
        return p3.y
    }
}