# Javascript运算符重载
javascript实现运算符重载功能
- 这里是列表文本重载的运算符:
+、-、*、/、%、**
- 这里是列表文本这里是列表文本需要类实现静态方法:
```
__add__、__plus__、__multiply__、__divide__、__mod__、__power__
```
### 项目简介
javascript一切皆对象的概念深入人心，但是它的运算符却只能应用于数值型和字符串，显得一切皆对象有点鸡肋。项目通过向javascript脚本代码的表达式注入__replace__方法实现对象运算符的重载。
### 用法
1. 通过script标签编写okayscript代码，okayscript作为代码容器，可被OOkay提取编译执行，例如
```
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>

</body>
<script type="text/javascript" src = "../../dist/ookay.js"></script>
<script type="text/javascript" src = "Point.js"></script>
<script type="text/okayscript">
    let p1 = new Point(2, 2)
    let p2 = new Point(3,1)
    let p3 = p1 + p2
    console.log('(x, y) of p3 :(' + p3.x + ',' + p3.y + ')')
</script>
</html>
```
在okayscript代码中，对象便可以使用运算符操作，运算符重载方法需要在类定义中指定静态__**__方法，如上例中的Point需要加法运算，我们需要重载—__add__方法，代码如下:
```
class Point {
    constructor (x, y) {
        this.x = x
        this.y = y
    }
    static __add__(p1, p2) {
        return new Point(p1.x + p2.x, p1.y + p2.y)
    }
}
```
2. 除了在okayscript中编写代码实现对象运算，还可以调用OOkay.__$__方法对需要对象运算的代码进行编译，例如：
```
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>

</body>
<script type="text/javascript" src = "../../dist/ookay.js"></script>
<script type="text/javascript" src = "Point.js"></script>
<script>
    OOkay.__$__(function(){
        let p1 = new Point(2, 2)
        let p2 = new Point(3,1)
        let p3 = p1 + p2
        console.log('(x, y) of p3 :(' + p3.x + ',' + p3.y + ')')
    })
</script>
</html>
```
3. 以上两点本质上是对代码块的编译，假如有一下场景：我们定义一个Rectangle类，Rectangle类的方法中需要进行运算符操作，就不能像上述方法手动将代码块提交给OOkay进行编译，我们的解决方案是让该类继承自OOKay类，例如：
```
class Rectangle extends OOkay {
    constructor (){
        super()
        this.left_top = new Point(0, 0)
        this.right_bottom = new Point(1, 1)
    }
    width () {
        let p3 = this.right_bottom - this.left_top
        return p3.x
    }
    height () {
        let p3 = this.right_bottom - this.left_top
        return p3.y
    }
}
```
Rectangle类继承自OOkay，Rectangle的width与height使用了Point的加法运算和减法运算，在其他地方就可以随意编写javascript，而无需再向OOkay提交代码编译了，例如：
```
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>

</body>
<script type="text/javascript" src = "../../dist/ookay.js"></script>
<script type="text/javascript" src = "Point.js"></script>
<script type="text/javascript" src = "Rectangle.js"></script>
<script>
    let rect = new Rectangle()
    console.log(rect.width()) // 1
    console.log(rect.height()) // 1
</script>
</html>
```
4. 第三点要求类必须是OOkay的对象，对于根类（最开始的类）不是OOkay的不适用，例如我们想要继承自Map类，显然不能修改Map类。我们可以在类构造器中OOkay.inject方法将对象提交给我们的OOkay进行编译，如上例可以修改为：
```
class Rectangle {
    constructor (){
        OOkay.inject(this)
        this.left_top = new Point(0, 0)
        this.right_bottom = new Point(1, 1)
    }
    width () {
        let p3 = this.right_bottom - this.left_top
        return p3.x
    }
    height () {
        let p3 = this.right_bottom - this.left_top
        return p3.y
    }
}
```
Rectange类无需继承自OOkay,只需在构造器中添加OOkay.inject(this)即可
5. 我们还可以通过修饰器进行注入编译，例如：
```
@inject('class')
class Rectangle{
    constructor (){
        this.left_top = new Point(0, 0)
        this.right_bottom = new Point(1, 1)
    }
    width () {
        let p3 = this.right_bottom - this.left_top
        return p3.x
    }
    height () {
        let p3 = this.right_bottom - this.left_top
        return p3.y
    }
}
```
通过@inject('class')对类进行修饰，也可以通过@inject('method')对方法进行修饰，从而无需编译整个类，例如：
```
class Rectangle{
    constructor (){
        this.left_top = new Point(0, 0)
        this.right_bottom = new Point(1, 1)
    }
    @inject('method')
    width () {
        let p3 = this.right_bottom - this.left_top
        return p3.x
    }
    @inject('method')
    height () {
        let p3 = this.right_bottom - this.left_top
        return p3.y
    }
    draw () {
        
    }
}
```
例子中width方法与height方法将被重新编译成okayscript,而draw方法不会
